import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainlayoutComponent } from './mainlayout/mainlayout.component';
import{ MainlayoutModule } from './mainlayout/mainlayout.module';
import { PokideskComponent } from './pages/pokidesk/pokidesk.component';
// import { SidebarComponent } from './components/sidebar/sidebar.component';
// import { SidebarModule } from './components/sidebar/sidebar.module';

const routes: Routes = [
  {path:'',
loadChildren:()=>import('./pages/login/login.module').then(m=>m.LoginModule)},
{path:'mainlayout',component:MainlayoutComponent},
{
  path: 'mainlayout',
  component: MainlayoutComponent,
  children: [
    {
      path: 'page-one',
      loadChildren: () =>import('./pages/page-one/page-one.module').then(m=>m.PageOneModule)
     },
    {
      path:'page-two',
      loadChildren:()=>import('./pages/page-two/page-two.module').then(m=>m.PageTwoModule)
    },
  {
    path:'pokidesk',
    loadChildren:()=>import('./pages/pokidesk/pokidesk.module').then(m=>m.PokideskModule)
  },
  {
    path:'sidebar',
    loadChildren:()=>import('./components/sidebar/sidebar.module').then(m=>m.SidebarModule)
  },]},
// {path:'pokidesk',component:PokideskComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes),
  // SidebarModule
MainlayoutModule,
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
