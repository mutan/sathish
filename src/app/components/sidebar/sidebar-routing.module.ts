import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar.component';

const routes: Routes = [
  
{path:'',component:SidebarComponent},
{path:'pages/pokidesk',
loadChildren:()=>import('../../pages/pokidesk/pokidesk.module').then(m=>m.PokideskModule)}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SidebarRoutingModule { }
