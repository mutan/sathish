import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  divert(){
    this.router.navigate(['mainlayout/pokidesk']);

  }
  divert1(){
    this.router.navigate(['mainlayout/page-one']);

  } divert2(){
    this.router.navigate(['mainlayout/page-two']);

  }
}
