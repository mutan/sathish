import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainpageComponent } from './mainpage/mainpage.component';


@NgModule({
  declarations: [MainpageComponent],
  exports:[MainpageComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    
  ]
})
export class MainModule { }
