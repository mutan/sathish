import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainlayoutRoutingModule } from './mainlayout-routing.module';
import { MainlayoutComponent } from './mainlayout.component';
import { MainModule } from '../main/main.module';




@NgModule({
  declarations: [MainlayoutComponent],
  exports:[
  ],
  imports: [
    CommonModule,
    MainlayoutRoutingModule,
    MainModule,

  ],
  providers: [
   
  ]
})
export class MainlayoutModule { }
