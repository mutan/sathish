import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmailValidator, FormBuilder, FormGroup, Validators } from '@angular/forms';
import{ServicesService} from '../../services.service';
import{ HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm:any= FormGroup;
  submitted = false;
  ers:any;
  loginsuccess:any;
  sucss:any;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private services:ServicesService,
    private http:HttpClient) { }

  ngOnInit(): void {
    this.loginForm=this.formBuilder.group({
      email:['',[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password:['',[Validators.required,Validators.minLength(6)]]
    });

    // LOGINPAGE BACK STOP
    
    this.loginsuccess=localStorage.getItem('success');
console.log(this.loginsuccess);
if(this.loginsuccess==null){

}else{
  this.router.navigate(['mainlayout']);

}
  }


  get f() { return this.loginForm.controls; }

onlogin(data:any){
  console.log(data);
  this.submitted=true;
  if(this.loginForm.invalid){
    return;
  }
  else{
var obj={email:data.email,password:data.password}
console.log(obj);
this.services.checkdata(obj).subscribe((a)=>{
  console.log('check',a);
  this.sucss=JSON.stringify(a);
  console.log('check1',this.sucss);
  // this.services.message(a);

  localStorage.setItem('success','123');
    this.router.navigate(['mainlayout',{title:this.sucss}]);

},
(err) => {console.log(err);
err=this.ers;
this.ers='* Invalid user name or password';
console.log(this.ers);
}
);
  }
  // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))


  }

}

