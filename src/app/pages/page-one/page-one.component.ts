import { Component, OnInit } from '@angular/core';
import{FormBuilder,FormGroup } from "@angular/forms"
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.component.html',
  styleUrls: ['./page-one.component.scss']
})
export class PageOneComponent implements OnInit {
character:any;
echaracter:any;
ary:any=[];
items:any;
  constructor(private formBuilder: FormBuilder,private modalService: NgbModal) { }

  ngOnInit(): void {
// MAIN FORM AREA
this.character=this.formBuilder.group({
  name:'',
  type:'',
  image:'',
});


// popup edit
this.echaracter=this.formBuilder.group({
  name:'',
  type:'',
  image:'',
});
  }


// SUBMIT AREA
onsave(val:any){
console.log(val);
this.ary.push(val);
console.log(this.ary);
this.character.setValue({name: '', type: '',image:''});
}

// DELETE BUTTON
ondel(a:any){
  console.log(a);
  this.ary.splice(a,1);
}


// EDIT BUTTON
onedit(a:any,val:any,i:any){
  this.modalService.open(a);
  console.log(a);
  console.log(val.name);
  console.log(i);

  this.items=i;
  this.echaracter.controls['name'].setValue(val.name);
  this.echaracter.controls['type'].setValue(val.type);
  // this.echaracter.controls['image'].setValue(val.image);
}


// UPDATE AREA
onsaved(){
  this.modalService.dismissAll()
  this.ary.splice(this.items,1,this.echaracter.value)
}
}
