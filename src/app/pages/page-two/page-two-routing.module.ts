import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from 'src/app/main/mainpage/mainpage.component';
import { PageTwoComponent } from './page-two.component';

const routes: Routes = [
  {path:'',component:PageTwoComponent},
  {path:'mainpage',component:MainpageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageTwoRoutingModule { }
