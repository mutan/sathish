import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokideskComponent } from './pokidesk.component';
import { PokidisplayComponent } from './pokidisplay/pokidisplay.component';

const routes: Routes = [
  {path:'',component:PokideskComponent,
  pathMatch: 'full'},
  {path:'pokidisplay',component:PokidisplayComponent}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PokideskRoutingModule { }
