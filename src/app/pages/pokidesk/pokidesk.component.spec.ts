import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokideskComponent } from './pokidesk.component';

describe('PokideskComponent', () => {
  let component: PokideskComponent;
  let fixture: ComponentFixture<PokideskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokideskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokideskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
