import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokideskRoutingModule } from './pokidesk-routing.module';
import { PokideskComponent } from './pokidesk.component';
import { SearchPipe } from '../../_pipe/search/search.pipe';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule }   from '@angular/forms';
import { PokidisplayComponent } from './pokidisplay/pokidisplay.component';                


@NgModule({
  declarations: [PokideskComponent,
    SearchPipe,
    PokidisplayComponent

    ],
  imports: [
    CommonModule,
    PokideskRoutingModule,
    NgbPaginationModule,
    FormsModule,
    
  ]
})
export class PokideskModule { }
