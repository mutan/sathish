import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokidisplayComponent } from './pokidisplay.component';

describe('PokidisplayComponent', () => {
  let component: PokidisplayComponent;
  let fixture: ComponentFixture<PokidisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokidisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokidisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
