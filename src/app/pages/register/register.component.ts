import { Component, OnInit } from '@angular/core';
import{ FormBuilder,FormGroup,Validator, Validators} from '@angular/forms'
import { HttpClient } from '@angular/common/http';
import{ServicesService} from '../../services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
registerform:any=FormGroup;
submitted = false;

  constructor(public formBuilder:FormBuilder,private http:HttpClient,private service:ServicesService
    ,private router: Router) { }

  ngOnInit(): void {

// FORM VALIDATION AREA

    this.registerform=this.formBuilder.group({
      name:['',Validators.required],
      email:['',[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
     password:['',[Validators.required,Validators.minLength(6)]],
      mobile:['',[Validators.required,Validators.pattern("^[0-9]*$"),
      Validators.minLength(10), Validators.maxLength(10)]],
    });
  }
get f(){return this.registerform.controls;}


// ON REGISTRATION AREA

onregs(val:any){
  this.submitted = true;
if(this.registerform.invalid){
  return;
}
console.log(val);


var dat={name:val.name,
  email:val.email,
  password:val.password,
mobile:val.mobile}


console.log(dat);
this.service.senddata(dat).subscribe((res)=>{
  console.log("send",res);
  this.router.navigate(['']);

})
}


// END AREA
}
